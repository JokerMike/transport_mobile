import 'package:flutter/material.dart';
//import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:transport_mobile/files/constantes.dart';
class CardItem extends StatelessWidget {
  final String title;
final String route; 
final IconData next;
  const CardItem({Key? key, required this.title, required this.route,required this.next}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
       Navigator.of(context).pushNamed(route);
      },
      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 78,
                          decoration: BoxDecoration(
                              color:  Colors.blue.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(20)),
                          child: Center(
                              child: Text(title,style: textStyle3,)),
                        ),
         ),
      );
    
     /* GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(route,arguments: null);
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom:25.0,left: 0,right: 0),
        child: Column(
          children: [
            Container(
              height: 60,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(child: Icon(next,size: 40,color: whitecolor,),radius: 40,backgroundColor: Colors.teal,),
                  Center(child: Text(title,style: TextStyle(color: Colors.black,fontSize: 20),))
                ],
              ),
            ),
              /* child: Center(
                child: ListTile(
                  //leading: Icon(Icons.ac_unit_sharp),
                  //CircleAvatar(backgroundColor:ikaccent,child: Icon(Icons.ac_unit_sharp),),
                  title: Text(title,style: textStyle2,),
                ),
              ), */
            ),
           
          ],
        ),
      ),
    ); */
  }
}