import 'package:flutter/material.dart';
class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          _createDrawerItem(icon: Icons.home,text: 'Accueil',),
          _createDrawerItem(icon: Icons.list_alt_sharp, text: 'Mes reservations',),
          _createDrawerItem(icon: Icons.person,text:'Mon compte'),
          _createDrawerItem(icon: Icons.settings, text: 'Parametres'),
          _createDrawerItem(icon: Icons.power_settings_new_rounded,text: 'Deconnexion'),
          
        ],
      ),
    );
  }
  Widget _createHeader() {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(color: Colors.purple.shade200),
      child: Stack(children: <Widget>[
        Positioned(
            bottom: 12.0,
            left: 86.0,
            top: 50,
            child: Text("APP TRANSPORT",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500))),
      ]));
}
Widget _createDrawerItem(
    {required IconData icon, required String text,  GestureTapCallback? onTap}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: ListTile(
      title: Row(
        children: <Widget>[
          Icon(icon),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text,style: TextStyle(fontSize: 20),),
          )
        ],
      ),
      onTap: onTap,
    ),
  );
    }}