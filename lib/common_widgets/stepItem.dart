import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/reservation.dart';
class stepItem extends StatelessWidget {
  final String title;
  final String route;
  final bool isDialog;
  final String? nom;
  const stepItem({
    Key? key,
    required this.title,
    required this.route,
    required this.isDialog,
    this.nom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom:20.0,top: 10),
      child: Container(
        height: 78,
        decoration: BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius: BorderRadius.circular(10)),
        child: Center(
            child: ListTile(
                title: Text(
                  title,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: blackcolor,
                  ),
                ),
                trailing: Icon(Icons.arrow_forward_ios_sharp),
                //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),

                )),
      ),
    );
  }
}

class stepItemFin extends StatelessWidget {
  final String title;
  final String route;
  final bool isDialog;
  final String? nom;
  const stepItemFin({
    Key? key,
    required this.title,
    required this.route,
    required this.isDialog,
    this.nom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
  var res=  Provider.of<Reservation>(context,listen:false);
    return Padding(
      padding: const EdgeInsets.only(bottom:15.0),
      child: Container(
        height: 60,
        decoration: BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius: BorderRadius.circular(10)),
        child: Center(
            child: ListTile(
                title:
                 Text(
                  title,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: blackcolor,
                  ),
                ),
                trailing: Icon(Icons.arrow_forward_ios_sharp),
                //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),

                )),
      ),
    );
  }
}
