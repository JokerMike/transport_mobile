import 'package:flutter/material.dart';
import 'package:transport_mobile/files/constantes.dart';


class EBDesign extends StatelessWidget {

  final String text;
  final VoidCallback press;

  EBDesign({required this.text, required this.press});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: buttonColor,
          onPrimary: Colors.white,
          textStyle: TextStyle(
            fontFamily:'SignikaNegative',
              color: Colors.black, fontSize: 15),
        ),
        onPressed:()=>{
          press
        },
        child: Text(text));
  }
}

class LoginButton extends StatelessWidget {
  final Color color;
  final String text;
  final VoidCallback? press;

  LoginButton({required this.text, required this.press,required this.color});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color,
          onPrimary: Colors.white,
          textStyle: TextStyle(
            fontFamily:'SignikaNegative',
              color: Colors.black, fontSize: 20),
        ),
        onPressed:()=>{
            Navigator.of(context).pushNamed("/accueil")

        },
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(text),
        ));
  }
}

class LogoutButton extends StatelessWidget {
  final Color color;
  final String text;
  final VoidCallback press;

  LogoutButton({required this.text, required this.press,required this.color});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color,
          onPrimary: Colors.white,
          textStyle: TextStyle(
            fontFamily:'SignikaNegative',
              color: Colors.black, fontSize: 20),
        ),
        onPressed:()=>{
          press
        },
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(text),
        ));
  }
}
class EBTextButton extends StatelessWidget {
  final String title;
  final Color color ;
  const EBTextButton({ Key? key,required this.title,required this.color }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(

 child: Padding(
   padding: const EdgeInsets.all(8.0),
   child: Text(title),
 ),
	
    style: TextButton.styleFrom(

      primary: color,	
      shape: const BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
	
    ),
	
    onPressed: () {
	
      print('Pressed');
	
    }, 
	
  );
  }
}
