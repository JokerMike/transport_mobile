import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;
  final bool obscureText;
//final TextInputType textInputType;
// required this.textInputType
  const Input({Key? key, required this.controller, required this.placeholder,  required this.obscureText})

      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(top: 15.0, left: 25, right: 25, bottom: 10),
      child: TextFormField(
        textAlign: TextAlign.justify,
        obscureText: obscureText,
        decoration: InputDecoration(
          fillColor: Colors.red,
            hintText: placeholder,
            hintStyle: TextStyle(color: Colors.black),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black38, width: 2.0),
             // borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 2.0),
             //  borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            focusedErrorBorder:const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 2.0),
              // borderRadius: BorderRadius.all(Radius.circular(15)),
            ),

            errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 2))),
      ),
    );
  }
}

class NoDecorationInput extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;
  final bool obscureText;
  final VoidCallback? ontap;
  final IconData? iconData;
//final TextInputType textInputType;
// required this.textInputType
  const NoDecorationInput({Key? key, required this.controller, required this.placeholder,  required this.obscureText,required this.iconData, this.ontap})

      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(top: 15.0, left: 15, right: 25, bottom: 10),
      child: TextFormField(
        onTap: (){
          ontap;
        },
        obscureText: obscureText,
        decoration: InputDecoration(
          
            labelText: placeholder,            
),
      ),
    );
  }
}


class PrefixInput extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;
  final bool obscureText;
  final VoidCallback? ontap;
  final IconData iconData;
//final TextInputType textInputType;
// required this.textInputType
  const PrefixInput({Key? key, required this.controller, required this.placeholder,  required this.obscureText,required this.iconData, this.ontap})

      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(top: 15.0, left: 25, right: 25, bottom: 10),
      child: TextFormField(
        onTap: (){
          ontap;
        },
        textAlign: TextAlign.justify,
        obscureText: obscureText,
        decoration: InputDecoration(
            hintText: placeholder,
          prefixIcon: Icon(iconData),
            hintStyle: TextStyle(color: Colors.blue.shade700),
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black38, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.lightBlueAccent, width: 2.0),
               borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            focusedErrorBorder:const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 2.0),
               borderRadius: BorderRadius.all(Radius.circular(10)),
            ),

            errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 2))),
      ),
    );
  }
}
