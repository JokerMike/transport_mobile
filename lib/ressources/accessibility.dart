import 'package:flutter/services.dart';

class Accessibility{
  String isEnable="false";
  static const platform = const MethodChannel('executecodeUssd');

   static Future<String> accessbilityEnable() async {
    String _message;
    try {
      bool result = await platform.invokeMethod('isEnable');
      _message = result.toString();
    } on PlatformException catch (e) {
      _message = "Error message : ${e.message}.";
    }
    return _message;
  }

 static void openAccessibility() async
{
  try {
      bool result = await platform.invokeMethod('openAccessibility');
    } on PlatformException catch (e) {
    }

}
}