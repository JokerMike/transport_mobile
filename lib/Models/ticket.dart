class Ticket {
  //final int id;
  final String numero;
  final String agence;
  final String date;
  final String ligne;
  final String place;
 

    Ticket({
    required this.place, 
     required this.ligne,
    required this.numero,
    required this.agence,
    required this.date,
  });

  
Map<String, dynamic> toMap() {
    return {'numero': numero, 'agence': agence, 'date': date, 'ligne': ligne};
  }


}