import 'dart:async';

import 'package:flutter/widgets.dart';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:transport_mobile/Models/ticket.dart';

class DatabaseHandler{
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path,'ticket_database.db'),
      onCreate: (database, version) async {
        await database.execute
        (
'CREATE TABLE tickets(id INTEGER PRIMARY KEY AUTOINCREMENT,agence TEXT NOT NULL,date TEXT NOT NULL,numero TEXT NOT NULL,ligne TEXT NOT NULL)',
        );
      },
      version: 1,
    );
  }

  //inserer un ticket
  Future<int> insertTicket(Ticket ticket) async {
    int result = 0;
    final Database db = await initializeDB();
    
      result = await db.insert('tickets', ticket.toMap());
    
    return result;
  }

  //lister les tickets

  Future<List<Ticket>> listTickets() async {
    final Database db = await initializeDB();
    final List<Map<String, dynamic>> maps = await db.query('tickets');
    print("ok"+maps.toString());
    return List.generate(maps.length, (i) {
      return Ticket(
       // id: maps[i]['id'],
        agence: maps[i]['agence'],

        date: maps[i]['date'], 
        numero: maps[i]['numero'],
        ligne: maps[i]['ligne'], 
        place:maps[i]['place']
      );
    });
   
  }

  //supprimer les tickets

  Future<void> deleteTickets(int id) async {
    final db = await initializeDB();
    await db.delete(
      'tickets',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
void main() async {
  // Avoid errors caused by flutter upgrade.
  // Importing 'package:flutter/widgets.dart' is required.
  WidgetsFlutterBinding.ensureInitialized();
  // Open the database and store the reference.
  final database = openDatabase(
    join(await getDatabasesPath(), 'ticket_database.db'),
    onCreate: (db, version) {
      return db.execute(
        'CREATE TABLE ticket(id INTEGER PRIMARY KEY AUTOINCREMENT,agence TEXT,date TEXT,numero TEXT,ligne TEXT,heure TEXT)',
      );
    },

    version: 1,
  );

}