
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:transport_mobile/ressources/api.dart';


class ReservationVendeur with ChangeNotifier {
  String telephone = "";
  late String mobileMoney="";
  late String secretCode ="";
  String ticket="";
  String codeUssd="";
  List<Map<String, dynamic>> _items = [];
   
dynamic element;
dynamic operatorInfo;
  void init(){
    this.telephone="";
    this._items=[];
    this.mobileMoney="";
    this.secretCode ="";
    this.element=null;
    this.operatorInfo=null;
  }
  String get phone {
    return telephone;
  }

  String get mobile {
    return mobileMoney;
  }

dynamic get elementItem{
  return  element;
}
  bool isPhone() {
    if (this.phone == "") {
      return false;
    } else {
      return true;
    }
  }

String getUssd(String code){
  var ussd=operatorInfo['ussd'].toString();
  ussd=ussd.replaceAll("Numero", operatorInfo['numero'].toString()).replaceAll("Montant",montant()).replaceAll("Pin",code);
  return ussd;
}
bool isElement() {
    if (this.element!=null) {
      return true;
    } else {
      return false;
    }
  }

bool isMobile() {
    if (this.mobileMoney !="") {
      return true;
    } else {
      return false;
    }
  }

  bool isPassword() {
    if (this.secretCode !="") {
      return true;
    } else {
      return false;
    }
  }
void setAgencySelected(dynamic el){
  this.element=el;
  notifyListeners();
}
List getAgencies(){
  return _items;
}
String montant(){
  var montant=(_items[0]['frais']+_items[0]['prix'])*int.parse(ticket);
  return montant.toString();
}
  void setPhone(String phone) {
    
    this.telephone = phone;
    notifyListeners();
  }

void setOperatorInfo(dynamic data) {
    this.operatorInfo = data;
    notifyListeners();
  }

  void setMobileMoney(String mobile) {
    this.mobileMoney = mobile;
    notifyListeners();
  }
  void setPassword(String password) {
    this.secretCode = password;
    notifyListeners();
  }
  Future<String> getTicket(
    String agence,
    String villeDepart,
    String villearriver,
    String datedepart,
    String place,
    String poids,
    String token,
  ) async {
    try {
          final response = await Api.getTicket(agence,
          villeDepart, villearriver, datedepart, place, poids,token);
          print(response);
          String validate = response["status"];
          if (validate.toLowerCase() == "success" && response["data"]['find'].length > 0) {
          ticket=response["data"]["ticket"];
          final data = response["data"]["find"] as List;
          this._items=[];
          //List<Agency> tmp = [];
          data.forEach((element) {
           // print(element.toString());
            _items.add(element);
          });

        notifyListeners();
        return "success";
      } else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return response["status"];
      } else {
        notifyListeners();
        return "error";
      }
    } catch (err) {
      throw err;
    }
  }
  Future<dynamic> madeTransaction(

    String  operator,
    String ticketNumber,
    String receiverPhone,
    String ticketId,
    String token,
    String name
  ) async {
    try {
      final response = await Api.madeTransaction(operator, ticketNumber, receiverPhone, ticketId,name,token);
      //    print(token);
    //   print("result depuis db:"+response.toString());
      String validate = response["status"];
      if (validate.toLowerCase() == "success") {
        notifyListeners();
        return response;
      } else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return response["status"];
      } else {
        notifyListeners();
        return "error";
      }
    } catch (err) {
      throw err;
    }
  }

  Future<String> getOperatorInfo(
String reseau,
String token,
    
  ) async {
    try {
      final response = await Api.operatorInfo(reseau,token);
         print(response);
      String validate = response["status"];
      if (validate.toLowerCase() == "success") {
       setOperatorInfo(response['data']);
        return "success";
      } else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return response["status"];
      } else {
        notifyListeners();
        return "error";
      }
    } catch (err) {
      throw err;
    }
  }
}

