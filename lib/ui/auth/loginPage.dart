import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/common_widgets/horizontalLine.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/Auth.dart' as models;
import 'package:transport_mobile/ressources/accessibility.dart';
import 'package:transport_mobile/ressources/connectivity.dart';

class Login extends StatefulWidget {
  static const routeName = "/connexion";

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isLoggin = false;
  bool invalidConnexion=false;

  TextEditingController username = new TextEditingController();

  TextEditingController password = new TextEditingController();

  final GlobalKey<FormState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return ChangeNotifierProvider<models.Auth>(
        create: (context) => models.Auth(),
        child: Consumer<models.Auth>(builder: (context, myModel, child) {
          return Scaffold(
            backgroundColor: Colors.purple.shade400,
            body: SafeArea(
              child: ListView(
                children: [
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Stack(
                      children: [
                        Center(
                            child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 90,
                        )),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0, left: 35),
                          child: Container(
                              height: 155,
                              child: Image.asset(pathImage + "tof1.png")),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: size.height * 0.66,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                    ),
                    child: Form(
                        key: _key,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: HorizontalOrLine(
                                  height: 15, label: "Connexion"),
                            ),
                          invalidConnexion?Center(child: Text("informations de connexion invalides",style: TextStyle(color: Colors.red,fontSize: 15),),):Padding(padding: EdgeInsets.zero),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0, left: 25, right: 25, bottom: 5),
                              child: TextFormField(
                                textAlign: TextAlign.justify,
                                keyboardType: TextInputType.phone,
                                obscureText: false,
                                controller: username,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "veuillez remplir ce champ";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    fillColor: Colors.red,

                                    hintText: "Numero de telephone",
                                    hintStyle: TextStyle(color: Colors.black),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black38, width: 2.0),
                                      // borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black, width: 2.0),
                                      //  borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    focusedErrorBorder:
                                        const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.red, width: 2.0),
                                      // borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2))),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0, left: 25, right: 25, bottom: 10),
                              child: TextFormField(
                                textAlign: TextAlign.justify,
                                controller: password,
                                obscureText: true,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "veuillez remplir ce champ";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                    fillColor: Colors.red,
                                    hintText: "Mot de passe",
                                    hintStyle: TextStyle(color: Colors.black),
                                    enabledBorder: const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black38, width: 2.0),
                                      // borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    focusedBorder: const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.black, width: 2.0),
                                      //  borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    focusedErrorBorder:
                                        const OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.red, width: 2.0),
                                      // borderRadius: BorderRadius.all(Radius.circular(15)),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.red, width: 2))),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, left: 50, right: 50, bottom: 10),
                              child: GestureDetector(
                                onTap: () async {
                                 await Connexion.isConnected().then((value){
                                   if(value=="false")
                                  {
                                    AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Internet',
                          desc: "connectez vous a internet",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                                  }else{
                                     if (_key.currentState!.validate()) {
                                    setState(() {
                                      isLoggin = true;
                                      invalidConnexion=false;
                                    });
                                    myModel
                                        .login(username.text, password.text)
                                        .then((val) async {
                                            print("role"+val['role'].toString());
                                        await Accessibility.accessbilityEnable().then((value) async {
                                print(value);
                                if(value=="false"){
                                Navigator.of(context).pushNamed("/accessibility");
                                }else{
                                   if(val['role']=="Voyageur"){
                                         Navigator.of(context).pushNamed("/accueil");}
                                         if(val['role']=="Vendeur"){
                                          Navigator.of(context).pushNamed("/vendeur");
                                       }
                                         else{
                                        setState(() {
                                          invalidConnexion=true;
                                          isLoggin = false;
                                        }); 
                                       }
                                }
                              });
                                    });
                                  } 
                                  }
                                 });
                                  
                                   // Navigator.of(context).pushNamed("/accueil");
                                  
                                  // Navigator.of(context).pushNamed("/accueil");
                                },
                                child: Container(
                                  height: 55,
                                  decoration: BoxDecoration(
                                      color: isLoggin?Colors.grey:Colors.purple.shade400,
                                      borderRadius: BorderRadius.circular(50)),
                                  child: Center(
                                      child: isLoggin
                                          ? CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(Colors.white) ,)
                                          : Text(
                                              "Connexion",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 25),
                                            )),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 18,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamed("/register");
                                    },
                                    child: Text("créer un compte",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.green,
                                          decoration: TextDecoration.underline,
                                        ))),
                               /*  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamed("/register");
                                    },
                                    child: Text("Mot de passe oublié",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.red,
                                          decoration: TextDecoration.underline,
                                        ))), */
                              ],
                            )
                            /*  Button.LoginButon(text: "Me Connecter", press:(){
                    Navigator.of(context).pushNamed("/accueil");
                  }) */
                          ],
                        )),
                  )
                ],
              ),
            ),
          );
        }));
  }
}
