import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/providers/Auth.dart' as models;
import 'package:transport_mobile/ressources/accessibility.dart';
import 'package:transport_mobile/ressources/connectivity.dart';

class Register extends StatefulWidget {
  static const routeName = "/register";

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  TextEditingController lastName = new TextEditingController();

  TextEditingController firstName = new TextEditingController();

  TextEditingController phoneNumber = new TextEditingController();

  TextEditingController email = new TextEditingController();

  TextEditingController password = new TextEditingController();

  TextEditingController confirmPassword = new TextEditingController();

  final GlobalKey<FormState> _key = GlobalKey();
  clearForm(){
   setState(() {
      lastName.text="";
    firstName.text="";
    phoneNumber.text="";
    email.text="";
   password.text="";
    confirmPassword.text="";
   });
  }
  bool isregistrering=false;
  @override
  Widget build(BuildContext context) {
    var model=Provider.of<models.Auth>(context, listen: false);
     return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: ListView(
        children: [
          Center(
              child: CircleAvatar(
            child: Text(
              "R",
              style: TextStyle(
                fontSize: 70,
                fontWeight: FontWeight.bold,
              ),
            ),
            radius: 50,
          )),
          Form(
            key: _key,
              child: Column(
            children: [
              Padding(
                padding:const EdgeInsets.only(top: 10.0, left: 15, right: 25, bottom: 5),
                child: TextFormField(
                  controller: lastName,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Nom de famille",
                  ),
                ),
              ),
              Padding(
                padding:const EdgeInsets.only(top:5, left: 15, right: 25, bottom:5),
                child: TextFormField(
                  controller: firstName,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Prenom(s)",
                  ),
                ),
              ),
              Padding(
                padding:const EdgeInsets.only(top:5, left: 15, right: 25, bottom:5),
                child: TextFormField(
                  controller: phoneNumber,
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                     if (value.length!=8) {
                      return "ce champ doit prendre 8 caracteres";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Telephone",
                  ),
                ),
              ),
               Padding(
                padding:const EdgeInsets.only(top:5, left: 15, right: 25, bottom: 5),
                child: TextFormField(
                  controller:email,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Email",
                  ),
                ),
              ),
               Padding(
                padding:const EdgeInsets.only(top:5, left: 15, right: 25, bottom: 5),
                child: TextFormField(
                  controller: password,
                 obscureText: true,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "Mot de passe",
                  ),
                ),
              ),
               Padding(
                padding:const EdgeInsets.only(top:5, left: 15, right: 25, bottom: 5),
                child: TextFormField(
                  controller: confirmPassword,
                 obscureText: true,
                  validator: (value) {
                      if (value!.isEmpty) {
                      return "Champ obligatoire";
                    }
                    if (value!=password.text) {
                      return "mot de passe non identiques";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    labelText: "confirmer le mot de passe",
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(
                    top: 5.0, left: 50, right: 50, bottom:5),
                child: InkWell(
                  onTap: () async {
                    await Connexion.isConnected().then((value){
                                   if(value=="false")
                                  {
                                    AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Internet',
                          desc: "Connectez-vous a internet",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                                  }else{
                                    if(_key.currentState!.validate() && isregistrering==false){
                        setState(() {
                      isregistrering=true;
                    });
                    
                        model.register(lastName.text, password.text,firstName.text,email.text, phoneNumber.text)
                        .then((value) async {
                         setState(() {
                             isregistrering=false;
                         });
                          if(value=="success"){
                        await Accessibility.accessbilityEnable().then((value) async {
                                print(value);
                                if(value=="false"){
                                Navigator.of(context).pushNamed("/accessibility");
                                }else{
                            Navigator.of(context).pushNamed("/accueil");
                          }});}
                          
                          else{
                           AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Erreur',
                          desc: value['message'],
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                          }
                        });
                    }
                                  }
    
                   }); },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: isregistrering==true?Colors.grey:Colors.red,
                        borderRadius: BorderRadius.circular(50)),
                    child: Center(
                        child: isregistrering==false? Text(
                      "Creer le compte",
                      style: TextStyle(color: Colors.white, fontSize: 23),
                    ):Row(
                      children: [
                        SizedBox(width: 20,),
                        CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
                        SizedBox(width: 10,),
                        Text("enregistrement",style: TextStyle(color: Colors.white,fontSize: 18),)
                      ],
                    )),
                  ),
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }}