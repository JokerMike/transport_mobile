import 'package:flutter/material.dart';
import 'package:transport_mobile/Models/sqlite.dart';
import 'package:transport_mobile/Models/ticket.dart';
import 'package:transport_mobile/files/constantes.dart';


class ListTickets extends StatefulWidget {
  static const routeName = "/listTickets";
  const ListTickets({Key? key}) : super(key: key);

  @override
  _ListTicketsState createState() => _ListTicketsState();
}

class _ListTicketsState extends State<ListTickets> {

late DatabaseHandler handler;

Future <List>getTicket() async{
  var tickets=await handler.listTickets();
  return tickets.toList();
}
@override
void initState() { 
  this.handler = DatabaseHandler();
  super.initState();
  
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List>(
        future: getTicket(),
        builder: (context, projectSnap) {
          if (projectSnap.data == null) {
            return Center(child: CircularProgressIndicator());
          }
          return projectSnap.data!.length==0
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Icon(
                        Icons.list,
                        size: 150,
                        color: Colors.grey.shade400,
                      ),
                      
                    ),
                    Text("Aucun ticket",style: textStyle1,)
                  ],
                )
              : displayTicket(projectSnap.data as List);
        },
      ),        floatingActionButton: FloatingActionButton(
          tooltip: "Recuperer la liste de vos tickets en ligne",
          backgroundColor: Colors.pink,
          onPressed:(){

        },
        child: Icon(Icons.download,color: Colors.white,),),
    );
  }


  Widget displayTicket(List items){
  List<GestureDetector>list=[];
   
   for(int i=0;i<items.length;i++){
      list.add( GestureDetector(
              onTap: () {
              //  Navigator.of(context).pushNamed(RouteName.GridViewBuilder);
              },
              onDoubleTap: ()
              {

              },
              child: Container(
                padding: const EdgeInsets.all(25),
               margin:EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                //color:data.color,
                color: Colors.pink.withOpacity(0.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(items[i].numero,
                          style: TextStyle(fontSize: 25, color: Colors.white),
                          textAlign: TextAlign.center),
                    ),
                        SizedBox(height: 10,),
                    Text(" ${items[i].agence}",style: textStyle3,),
                    Text(" ${items[i].ligne}",style: textStyle3),
                    Text(" ${items[i].date}",style: textStyle3),
                  ],
                ),
              )),
        );
        
    }
    return SafeArea(
      child: GridView.count(
      crossAxisCount: 1,
      childAspectRatio: (3 / 2),
      crossAxisSpacing: 5,
      mainAxisSpacing: 5,
      physics:BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0),
      children: list.reversed.toList()
    ));
  }
}
