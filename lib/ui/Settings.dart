import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/Auth.dart';
import 'package:transport_mobile/ressources/connectivity.dart';
class Parametres extends StatelessWidget {
  const Parametres({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
      Future<dynamic> showmodal(String text) {
    return showModalBottomSheet(
        context: context,
        builder: (context) => Container(
              height: 70,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: Row(
                  children: [
                    CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      text,
                      style: textStyle3,
                    )
                  ],
                ),
              ),
            ));
  }
    var provider=Provider.of<Auth>(context);
    return  Scaffold(
      body:SafeArea(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.only(top:28.0),
              child: Center(
                child:CircleAvatar(child: Icon(Icons.person,size: 60,color: Colors.white,),backgroundColor: Colors.grey.shade300,radius: 50,) ,
              ),
            ),
            SizedBox(height: 30,),
            Center(
              child:Text("ZOROBABEL koffi"),
            ),
            SizedBox(height: 20,),
           Expanded(
             child: GridView.count(
      crossAxisCount: 2,
      childAspectRatio: (3 / 2),
      crossAxisSpacing: 30,
      mainAxisSpacing: 50,
     // physics:BouncingScrollPhysics(),
      padding: EdgeInsets.all(3.0),
      children: [
        GestureDetector(
        child:  Container(
          color: Colors.amber,
          height:100,
          width: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Icon(Icons.lock,size: 50,color:Colors.white),
              SizedBox(height: 10),
              Text("changer mot de passe")
          ],
        ))
        ),
         GestureDetector(
        child:  Container(
          color: Colors.blue,
          height:100,
          width: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Icon(Icons.phone,size: 50,color:Colors.white),
              SizedBox(height: 10),
              Text("changer de numero")
          ],
        ))
        ),
         GestureDetector(
        child:  Container(
          color: Colors.teal,
          height:100,
          width: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Icon(Icons.delete,size: 50,color:Colors.white),
              SizedBox(height: 10),
              Text("supprimer mon compte")
          ],
        ))
        ),
         GestureDetector(
           onTap: () async {
              await Connexion.isConnected().then((value){
                                   if(value=="false")
                                  {
                                    AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Internet',
                          desc: "connectez vous a internet",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                                  }else{
                                     showmodal("déconnexion en cours");
                                    provider.deconnexion().then((value){
               if(value=="success"){
                 Navigator.pop(context);
                 Navigator.of(context).pushNamed("/connexion");
               }else{
                 Navigator.pop(context);
                 AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Erreur',
                          desc: "Deconnexion non reussie",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true

                        )..show();

               }
             });
                                  }
             
           });},
        child:  Container(
          color: Colors.red,
          height:100,
          width: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Icon(Icons.settings_power,size: 50,color:Colors.white),
              SizedBox(height: 10),
              Text("Deconnexion")
          ],
        ))
        )
      ]
    ),
           )
          ],
        ),
      ) ,
    );
  }
}