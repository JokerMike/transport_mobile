import 'package:flutter/material.dart';
import 'package:transport_mobile/files/constantes.dart';

class Success extends StatelessWidget {
  static const routeName = "/success";
  const Success({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.grey.shade300,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top:20.0,bottom: 20),
            child: CircleAvatar(
              radius: 90,
              backgroundColor: Colors.blue,
              child: Icon(Icons.thumb_up_rounded,size: 90,color: Colors.white,),
            ),
          ),
          Center(child: Text("Félicitations",style: textStyle1,),),
          Padding(
            padding: const EdgeInsets.only(left:15.0,top: 20,right: 15),
            child: Text("Votre paiement a été fait avec succès.Vous pourrez voir les informations du ticket dans votre liste.",textAlign: TextAlign.justify,style:TextStyle(fontSize: 18),),
          ),
          
          Padding(
                        padding: const EdgeInsets.only(top:20.0,left: 50,right: 50,bottom: 10),
                        child: GestureDetector(
                          onTap: (){
                           Navigator.of(context).pushNamed("/accueil"); 
                          },
                          child: Container(
                            height: 55,
                            decoration: BoxDecoration(
                                color:  Colors.pink,
                                borderRadius: BorderRadius.circular(50)),
                            child: Center(
                                child: Text("Page d'accueil",style: TextStyle(color: Colors.white,fontSize: 20),)),
                          ),
                        ),
         ),
        
        ],
      ),
    );
  }
}