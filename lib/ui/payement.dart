import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/common_widgets/stepItem.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/reservation.dart' as models;
import 'package:transport_mobile/providers/reservation.dart';
import 'package:transport_mobile/ressources/shared_preferences.dart';

class Payement extends StatefulWidget {
  static const routeName = "/paiement";
  @override
  _PayementState createState() => _PayementState();
}

class _PayementState extends State<Payement> {

  int _currentStep = 0;
  bool firststep = false;
  bool secondstep = false;
  bool thirdstep = false;
  var operatorInfo;
  String number="";
  String _token="";
  StepperType stepperType = StepperType.horizontal;
   TextEditingController password = new TextEditingController();
  var _formkey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController dateDepart = TextEditingController();
  TextEditingController villeDepart = TextEditingController();
  TextEditingController villeDestination = TextEditingController();
  TextEditingController bagagePoids = TextEditingController();
  TextEditingController nombreTickets = TextEditingController();


  void getnumber(){
    SharedPreferencesClass.restore("number").then((value){
      setState(() {
        number=value;
      });
    });
  }
void getToken(){
    SharedPreferencesClass.restore("token").then((value){
      setState(() {
        _token=value;
      });
    });
  }
  @override
  void initState() {
    getnumber(); 
    getToken();
    print(number);
    super.initState();
    
  }
  Future<dynamic> showmodal(String text) {
    return showModalBottomSheet(
        context: context,
        builder: (context) => Container(
              height: 70,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: Row(
                  children: [
                    CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      text,
                      style: textStyle3,
                    )
                  ],
                ),
              ),
            ));
  }

  List<Step> _steps() {
    List<Step> widgets = [
      Step(
        title: new Text('Infos'),
        content: Form(
          key: _formkey,
          child: Column(
            children: <Widget>[
              Center(
                child: Text(
                  "Informations de recherche",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              DateTimePicker(
                initialValue: dateDepart.text,
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                dateLabelText: 'Date de depart',
                onChanged: (val) {
                  setState(() {
                    dateDepart.text = val;
                    print("date1 :" + dateDepart.text);
                  });
                },
                validator: (val) {
                  if (dateDepart.text == "") {
                    return "ce champ est obligatoire";
                  }
                  return null;
                },
                onSaved: (val) {
                  setState(() {
                    dateDepart.text = val!;
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextFormField(
                  controller: villeDepart,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Ville de depart'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextFormField(
                  controller: villeDestination,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    return null;
                  },
                  decoration:
                      InputDecoration(labelText: 'Ville de destination'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: bagagePoids,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    var v = int.tryParse(value);

                    if (v == null) {
                      return "veuillez saisir une valeur correcte";
                    }
                    if (v < 0) {
                      return "la valeur ne peut etre negatif";
                    }
                    if (v == 0) {
                      return "veuillez verifier la valeur entre";
                    }
                    return null;
                  },
                  decoration:
                      InputDecoration(labelText: 'Poids des bagages(en kg)'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextFormField(
                  controller: nombreTickets,
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    var v = int.tryParse(value);

                    if (v == null) {
                      return "veuillez saisir une valeur correcte";
                    }
                    if (v < 0) {
                      return "la valeur ne peut etre negatif";
                    }
                    if (v == 0) {
                      return "veuillez verifier la valeur entre";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Nombre de tickets'),
                ),
              ),
            ],
          ),
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
      ),
      Step(
        title: new Text('Agence'),
        content: Column(
          children: <Widget>[
            Container(
                height: 150,
                width: 150,
                child: Image.asset(pathImage + "ticket.png")),
            GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Container(
                          height: 300,
                          child: ListView(
                            children: [
                              Form(
                                  child: Column(
                                children: [
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Center(
                                    child: Text(
                                      "Utilisateur du ticket",
                                      style: textStyle1,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 10.0,
                                        top: 10,
                                        left: 30,
                                        right: 30),
                                    child: GestureDetector(
                                      onTap: () {
                                        Provider.of<models.Reservation>(context,
                                                listen: false)
                                            .setPhone(number);
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        child: Center(
                                          child: Text(
                                            "Le ticket m'appartiendra",
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              )),
                            ],
                          ));
                    });
              },
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Container(
                  height: 78,
                  decoration: BoxDecoration(
                      color: Provider.of<models.Reservation>(context,
                                  listen: false)
                              .isPhone()
                          ? Colors.blue.shade300
                          : Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                      child: ListTile(
                    title:
                        Provider.of<models.Reservation>(context, listen: false)
                                .isPhone()
                            ? Center(
                                child: Text(
                                  
                                      Provider.of<models.Reservation>(context,
                                              listen: false)
                                          .phone,
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            : Text(
                                "Utilisateur final du ticket",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: blackcolor,
                                ),
                              ),

                    trailing:
                        Provider.of<models.Reservation>(context, listen: false)
                                .isPhone()
                            ? Padding(padding: EdgeInsets.zero)
                            : Icon(Icons.arrow_forward_ios_sharp),
                    //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),
                  )),
                ),
              ),
            ),
            GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                            height: 500,
                            child: Consumer<Reservation>(
                                builder: (_, res, __) => (ListView.builder(
                                    itemCount: res.getAgencies().length,
                                    itemBuilder: (context, index) =>
                                        GestureDetector(
                                          onTap: () {
                                            res.setAgencySelected(res
                                                .getAgencies()
                                                .elementAt(index));
                                            Navigator.pop(context);
                                            //  res.setAgency("NAGODE Transport");
                                            //   Navigator.of(context).pushNamed("/facture");
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                                height: 120,
                                                decoration: BoxDecoration(
                                                    border: Border.all(),
                                                    color: Colors.grey.shade200,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Text(
                                                        res
                                                            .getAgencies()
                                                            .elementAt(
                                                                index)['find']['agence']
                                                            .toUpperCase(),
                                                        style: textStyle3,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              5.0),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                  " Heure Depart"),
                                                              Text(
                                                                res.getAgencies().elementAt(
                                                                        index)['find'][
                                                                    'heureDepart'],
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .green,
                                                                    fontSize:
                                                                        15),
                                                              )
                                                            ],
                                                          ),
                                                          Column(
                                                            children: [
                                                              Text(
                                                                "${res.getAgencies().elementAt(index)['ticketprice']}FCFA/ticket",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .green,
                                                                    fontSize:
                                                                        17),
                                                              ),
                                                            ],
                                                          ),
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(" frais"),
                                                              Text(
                                                                "${res.getAgencies().elementAt(index)['ticketfrais']} FCFA",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .red
                                                                        .shade300,
                                                                    fontSize:
                                                                        15),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Text(
                                                        "Places restantes:${res.getAgencies().elementAt(index)['find']['place']}",
                                                        style: TextStyle(
                                                            color: Colors
                                                                .red.shade400,
                                                            fontSize: 20))
                                                  ],
                                                )),
                                          ),
                                        )))));
                      });
                },
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20.0, top: 10),
                  child: Container(
                    height: 78,
                    decoration: BoxDecoration(
                        color: Provider.of<models.Reservation>(context,
                                    listen: false)
                                .isElement()
                            ? Colors.blue.shade300
                            : Colors.grey.shade300,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                        child: ListTile(
                      title: Provider.of<models.Reservation>(context,
                                  listen: false)
                              .isElement()
                          ? Center(
                              child: Text(
                                "Agence: ${Provider.of<models.Reservation>(context, listen: false).element['find']['agence']}",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: blackcolor,
                                ),
                              ),
                            )
                          : Text(
                              "Selectionner une agence",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: blackcolor,
                              ),
                            ),

                      trailing: Provider.of<models.Reservation>(context,
                                  listen: false)
                              .isElement()
                          ? Padding(padding: EdgeInsets.zero)
                          : Icon(Icons.arrow_forward_ios_sharp),
                      //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),
                    )),
                  ),
                )),
          ],
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
      ),
      Step(
        title: new Text('Finalisation'),
        content: Column(
          children: <Widget>[
            GestureDetector(
                onTap: () {
                  showDialog<void>(
                    context: context,
                    barrierDismissible: false, // user must tap button!
                    builder: (BuildContext context) {
                      return AlertDialog(
                        // title:  Center(child: Text('Payer par',style: TextStyle(fontSize: 30),)),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  Provider.of<models.Reservation>(context,
                                          listen: false)
                                      .setMobileMoney("Togocel");
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.payment,
                                        color: Colors.green,
                                        size: 30,
                                      ),
                                     // SizedBox(width: 10),
                                      Text(
                                        'Togocom(Tmoney)',
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              InkWell(
                                onTap: () {
                                  Provider.of<models.Reservation>(context,
                                          listen: false)
                                      .setMobileMoney("MoovAfrica");
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.payment,
                                        color: Colors.yellow.shade700,
                                        size: 30,
                                      ),
                                     
                                      Text(
                                        'Moov Africa(Flooz)',
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.yellow.shade700,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        /* actions: <Widget>[
          TextButton(
            child: const Text('Approve'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ], */
                      );
                    },
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 15.0),
                  child: GestureDetector(
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                          color: Provider.of<models.Reservation>(context,
                                      listen: false)
                                  .isMobile()
                              ? Colors.blue.shade300
                              : Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                          child: ListTile(
                        title: Provider.of<models.Reservation>(context,
                                    listen: false)
                                .isMobile()
                            ? Center(
                                child: Text(
                                  "Payer par :" +
                                      Provider.of<models.Reservation>(context,
                                              listen: false)
                                          .mobile,
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color:blackcolor,
                                  ),
                                ),
                              )
                            : Text(
                                "Mode de payement",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: blackcolor,
                                ),
                              ),
                        trailing: Provider.of<models.Reservation>(context,
                                    listen: false)
                                .isMobile()
                            ? Padding(padding: EdgeInsets.zero)
                            : Icon(Icons.arrow_forward_ios_sharp),
                        //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),
                      )),
                    ),
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: GestureDetector(
                onTap: () {
                  showDialog<void>(
                    context: context,
                    barrierDismissible: true, // user must tap button!
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(
                            child: Text(
                          'Code secret',
                          style: TextStyle(fontSize: 30),
                        )),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              TextFormField(
                                controller: password,
                                textAlign: TextAlign.justify,
                                obscureText: true,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  fillColor: Colors.red,
                                  hintText: "Saisissez votre code secret",
                                  hintStyle: TextStyle(color: Colors.black),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black38, width: 2.0),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.black, width: 2.0),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                  ),
                                ),
                              ),
                              SizedBox(height: 20,),
                              ElevatedButton(
                                child: Text('Code saisi',style: TextStyle(fontSize: 20,color:Colors.white,)),
                                    style: ElevatedButton.styleFrom(
                              primary: Colors.purple,),
                                onPressed: () {
                                  Provider.of<models.Reservation>(context,
                                    listen: false)
                                .setPassword(password.text);
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                      color: Provider.of<models.Reservation>(context,
                                  listen: false)
                              .isPassword()
                          ? Colors.blue.shade300
                          : Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                      child: ListTile(
                    title:
                        Provider.of<models.Reservation>(context, listen: false)
                                .isPassword()
                            ? Center(
                                child: Text(
                                  "Code secret:****",
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: blackcolor,
                                  ),
                                ),
                              )
                            : Text(
                                "Code Secret",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: blackcolor,
                                ),
                              ),
                    trailing:
                        Provider.of<models.Reservation>(context, listen: false)
                                .isMobile()
                            ? Padding(padding: EdgeInsets.zero)
                            : Icon(Icons.arrow_forward_ios_sharp),
                    //Text("Agence :$nom",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: blackcolor,),),
                  )),
                ),
              ),
            ),
            Image.asset(pathImage + "payment.png"),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Finaliser votre paiement",
                  style: textStyle1,
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top:20,bottom: 10)
            ,child: RichText(
  text: TextSpan(
    text: 'Important: ',
    style: TextStyle(fontSize: 18,color: Colors.red,fontWeight:FontWeight.bold),
    children: const <TextSpan>[
      TextSpan(text: "La saisie de votre code secret permettra d'executer le payement par votre compte mobile.Nous ne le conservons pas.", style: TextStyle(color: Colors.black,fontSize: 15)),
     
    ],
  ),
)
            )
          ],
        ),
        isActive: _currentStep >= 0,
        state: _currentStep >= 2 ? StepState.complete : StepState.disabled,
      ),
    ];

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    switchStepsType() {
      setState(() => stepperType == StepperType.vertical
          ? stepperType = StepperType.horizontal
          : stepperType = StepperType.vertical);
    }

    tapped(step) {
      setState(() {
        //si c'est le premier step alors on verifie si les info sont saisies
        if (step == 0) {
          if (firststep) {
            this._currentStep = step;
          }
        }

        if (step == 1) {
          if (firststep) {
            this._currentStep = step;
          }
        }
      });
    }

    continued() {
      setState(() {
        //step 1
        if (this._currentStep == 0) {
          if (_formkey.currentState!.validate()) {
            print(villeDepart.text +
                " " +
                villeDestination.text +
                " " +
                dateDepart.text +
                " " +
                nombreTickets.text +
                " " +
                bagagePoids.text);
            showmodal("Recherche des agences");
            Provider.of<models.Reservation>(context, listen: false)
                .getAgency(villeDepart.text, villeDestination.text,
                    dateDepart.text, nombreTickets.text, bagagePoids.text,_token)
                .then((value) {
              Navigator.pop(context);
              if (value == "error") {
                setState(() {
                  firststep = false;
                });
                showDialog(
                    context: context,
                    builder: (_) => new AlertDialog(
                          title: Center(
                              child: new Text(
                            "Oups!!!",
                            style: TextStyle(color: Colors.red, fontSize: 25),
                          )),
                          content: new Text(
                              "Désolé, aucune des agences ne reponds a vos critères ",
                              style: TextStyle(fontSize: 20)),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Compris',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        ));
              } else {
                setState(() {
                  firststep = true;
                  this._currentStep += 1;
                });
              }
            }).catchError((onError) {});
            /* setState(() {
            firststep = true;
          }); */
          }
        }
            //step 2
        if(this._currentStep == 1)
        {
            if
            ( firststep && 
              Provider.of<models.Reservation>(context, listen: false).isElement() &&
              Provider.of<models.Reservation>(context, listen: false).isPhone()
             )
            {
              
              setState(() {
                secondstep=true;
                this._currentStep += 1;
              });
            }else{
               setState(() {
                secondstep=false;
              });
            }
        }
        //step 3
        if(this._currentStep == 2  && secondstep)
        {
            if
            ( secondstep   &&
              Provider.of<models.Reservation>(context, listen: false).isMobile() &&
              Provider.of<models.Reservation>(context, listen: false).isPassword()
             )
            {
              showmodal("Un instant");
               Provider.of<models.Reservation>(context, listen: false).getOperatorInfo(_token).then((value){
                 print(value);
                Navigator.pop(context);
                 if(value=="success")
                 {
                    setState(() {
                        thirdstep=true;
                         Navigator.of(context).pushNamed("/confirmPage");
                        });
                 }else
                 {
                   setState(() {
                      thirdstep=false;
                    });
                 }
               });
            }else{
            }
        }
       

      });
    }

    cancel() {
      _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
    }
    //var agences = Provider.of<models.Reservation>(context,listen:false);

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text('Payer un ticket',
              style:
                  TextStyle(color: Colors.purple, fontWeight: FontWeight.bold)),
          centerTitle: true,
        ),
        body: Consumer<Reservation>(
          builder: (_, res, __) => Container(
            child: Column(
              children: [
                Expanded(
                  child: Stepper(
                    type: stepperType,
                    physics: ScrollPhysics(),
                    currentStep: _currentStep,
                    onStepTapped: (step) => tapped(step),
                    onStepContinue: continued,
                    onStepCancel: cancel,
                    steps: _steps(),
                    controlsBuilder: (BuildContext context,
                        {VoidCallback? onStepContinue,
                        VoidCallback? onStepCancel}) {
                      return Row(
                        children: <Widget>[
                          ElevatedButton(
                              child: Text(
                                'SUIVANT',
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.purple,
                              ),
                              onPressed: onStepContinue),
                          SizedBox(
                            width: 20,
                          ),
                          TextButton(
                            onPressed: onStepCancel,
                            child: const Text(
                              'RETOUR',
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
