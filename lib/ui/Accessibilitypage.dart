import 'package:flutter/material.dart';
import 'package:transport_mobile/ressources/accessibility.dart';


class AccessibilityPage extends StatelessWidget {
  static const routeName = "/accessibility";
  const AccessibilityPage({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          SizedBox(
            height: 80,
          ),
          CircleAvatar(
            backgroundColor: Colors.blue,
            radius: 60,
            child: Icon(
              Icons.accessibility,
              size: 60,
              color: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              "Les services de payement necessitent l'usage de l'accessibilite,veuillez l'activer.",
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 22),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                onPressed: () {
                  Accessibility.openAccessibility();
                },
                style: ElevatedButton.styleFrom(primary: Colors.pink),
                child: Text(
                  "Activer l'accesibilité",
                  style: TextStyle(fontSize: 18,color: Colors.white),
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                onPressed: () {
                  Accessibility.accessbilityEnable().then((value) async {
                    if (value == "true") {
                      await Navigator.of(context)
                          .pushReplacementNamed("/accueil");
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) => SimpleDialog(
                                title:
                                    const Center(child: Text("Accessibilité")),
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "L'accessibilité n'est pas actif..veuillez l'activer.",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.pink),
                                        child: Text("compris",style: TextStyle(color: Colors.white),)),
                                  )
                                ],
                              ));
                    }
                  });
                },
                style: ElevatedButton.styleFrom(primary: Colors.blue),
                child: Text("Continuer", style: TextStyle(fontSize: 18,color: Colors.white))),
          )
        ],
      ),
    );
  }
}
