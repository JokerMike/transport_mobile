import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/reservation.dart' as providers;
// ignore: import_of_legacy_library_into_null_safe
import 'package:sim_service/sim_service.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:sim_service/models/sim_data.dart';

class Accueil extends StatefulWidget {
  const Accueil({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  _AccueilState createState() => _AccueilState();
}

class _AccueilState extends State<Accueil> {
  dynamic _simData;
  bool gettingData = false;
  String isNotSim =
      "Votre telephone ne supportant pas le choix de carte sim vous ne pourrez faire des transaction que par la Sim 1.";

  Future<void> initPlatformState() async {
    dynamic simData;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      setState(() {
        gettingData = true;
      });
      simData = await SimService.getSimData;
    } on PlatformException {
      // print(simData);
    }
    //print(simData.carrierName);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      gettingData = false;
      _simData = simData;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    var res = Provider.of<providers.Reservation>(context, listen: false);
    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  height: widget.size.height * 0.35,
                  width: double.infinity,
                  color: Colors.pink.shade600,
                  child: Column(
                    children: [
                      SafeArea(
                          child: Padding(
                        padding: EdgeInsets.only(top: 15, left: 10),
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 65,
                        ),
                      )),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 15, right: 50, left: 30),
                          child: Column(
                            children: [
                              Text(
                                "Bienvenue sur E-billet",
                                style: textStyle2,
                              ),
                              // Text("",style: textStyle2)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          height: widget.size.height / 5,
                          child: Image.asset(pathImage + "phone.png"),
                        ),
                        Column(
                          children: [
                            Text(
                              "Bienvenue",
                              style: textStyle1,
                            ),
                            Text("Payer vos tickets en ligne ici")
                          ],
                        )
                      ],
                    ),

                    MenuItem(
                      res: res,
                      text: "Rechercher une agence",
                    ),
                    GestureDetector(
                        onTap: () {
                          res.init();
                          Navigator.of(context).pushNamed("/paiement");
                        },
                        child: MenuItem(
                          res: res,
                          text: "payer un ticket de voyage",
                        )),
                    MenuItem(
                      res: res,
                      text: "Annuler une reservation",
                    ),

                    //  haveSimCard(),
                  ],
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  haveSimCard() {
    if (_simData == null) {
      return Column(
        children: [
          /*  Center(
            child: Container(
              height:widget.size.height/5,
              child: Image.asset(pathImage+"light.png")),
          ), */
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 10, top: 10),
            child: Container(
              child: Text(
                isNotSim,
                textAlign: TextAlign.justify,
                style: textStyle4,
              ),
            ),
          )
        ],
      );
    } else {
      return Padding(padding: EdgeInsets.zero);
    }
  }
}

class MenuItem extends StatelessWidget {
  final String text;
  const MenuItem({
    Key? key,
    required this.res,
    required this.text,
  }) : super(key: key);

  final providers.Reservation res;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 30, left: 30, right: 30),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.pink, borderRadius: BorderRadius.circular(50)),
        height: 50,
        // color: Colors.amber,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: 5,
            ),
            CircleAvatar(
              child: Icon(
                Icons.check,
                color: Colors.white,
              ),
              backgroundColor: Colors.grey,
            ),
            SizedBox(
              width: 18,
            ),
            Text(
              text,
              style: textStyle5,
            )
          ],
        ),
      ),
    );
  }
}
