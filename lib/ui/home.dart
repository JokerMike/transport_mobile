

import 'package:flutter/material.dart';
import 'package:transport_mobile/ui/Settings.dart' as ui;

import 'package:transport_mobile/ui/accueil.dart' as ui;
import 'confirmPage.dart'as ui;
import 'listTickets.dart' as ui;

class Home extends StatefulWidget {
  static const routeName = "/accueil";

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController username = new TextEditingController();
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size siza = MediaQuery.of(context).size;
     final tabs = [
     ui.Accueil(size: siza),
     ui.ListTickets(),
    ui.Parametres()
    ];
    
    return Scaffold(
      body: tabs.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
       // backgroundColor: Colors.pink,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Mes tickets',
          ),
          
          BottomNavigationBarItem(
            icon: Icon(
              Icons.settings,
            ),
            label: 'Parametres',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      ),
    );
  }
}



class Item extends StatelessWidget {
  final String text;
  final IconData iconData;

  const Item({required this.text, required this.iconData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0, left: 5, right: 2),
      child: Container(
        height: 100,
        width: 150,
        child: Card(
          color: Colors.blue.shade400,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //CircularProgressIndicator(),
              Center(child: CircleAvatar(child: Icon(Icons.home))),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 5),
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
