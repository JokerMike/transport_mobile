
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/Models/sqlite.dart';
import 'package:transport_mobile/Models/ticket.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/reservation.dart' as models;
// ignore: import_of_legacy_library_into_null_safe
import 'package:sim_service/sim_service.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:sim_service/models/sim_data.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sim_info/sim_info.dart';
import 'package:transport_mobile/ressources/accessibility.dart';
import 'package:transport_mobile/ressources/shared_preferences.dart';


class Confirm extends StatefulWidget {
  static const routeName = "/confirmPage";
  const Confirm({Key? key}) : super(key: key);

  @override
  _ConfirmState createState() => _ConfirmState();
}


class _ConfirmState extends State<Confirm> {
dynamic  _simData;
  bool gettingData = false;
 late DatabaseHandler handler;
  String _requestCode = "*555#";
  String _responseCode = "";
  String _responseMessage = "";
  int sim = 15;
  List <Ticket>listTickets=[];
  String simId="";
  String _token="";
  dynamic responseRes;
  static const platform = const MethodChannel('executecodeUssd');

getToken()async{
 await SharedPreferencesClass.restore("token").then((value) {
    setState(() {
         _token=value;
       });
 });
   
}

  Future<String> executecode(int subscriptionId, String code) async {
    if (await Permission.phone.request().isGranted) {
      try {
        await platform.invokeMethod("execute",
            {'subscriptionId': subscriptionId, 'code': code}).then((value) {
          setState(() {
            _responseMessage = value;
          });
        });
      } catch (e) {
        print(e);
      }
    }
    print(_responseMessage);
    return _responseMessage;
  }
 Future<void> initPlatformState() async {
   dynamic simData;

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      setState(() {
        gettingData = true; 
      });
      simData = await SimService.getSimData;
    } on PlatformException {
      print(simData);
    }
    //print(simData.carrierName);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      gettingData = false;
      _simData = simData;
    });
  }
  @override
  void initState() {
     this.handler = DatabaseHandler();
      initPlatformState();
   getToken();
    super.initState();
   
//initPlatform();
  }
 Future<String> codeExecution(String code) =>
      executecode(sim, code);
  @override
  Widget build(BuildContext context) {
    var pr = new ProgressDialog(context);
    pr.style(
        message: 'validation de paiement....',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: Image(
          image: AssetImage("images/loader.gif"),
        ));
var prov=  Provider.of<models.Reservation>(context, listen: false);
    Size size = MediaQuery.of(context).size;
return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.grey.shade200,
                title: Text(
                  "Confirmation",
                  style: TextStyle(fontSize: 30),
                ),
                centerTitle: true,
                automaticallyImplyLeading: true,
                elevation: 0,
              ),
              backgroundColor: Colors.grey.shade200,
              body: SingleChildScrollView(
                child: SafeArea(
                  child: Container(
                    height: size.height * 0.91,
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      height: size.height,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.circular(30.0),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: CircleAvatar(
                              backgroundColor: Colors.black54,
                              child: Icon(
                                Icons.directions_bus,
                                color: Colors.white,
                                size: 40,
                              ),
                              radius: 30,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                              child: Text(
                            prov.element['find']['ligne'].toString().toUpperCase(),
                            style: textStyle1,
                          )),
                          SizedBox(
                            height: 20,
                          ),
                          ConfirmItem(
                            label: "Agence de voyage",
                            value: prov.element['find']['agence'].toString(),
                          ),
                          ConfirmItem(
                            label: "Prix unitaire du ticket",
                            value: "${prov.element['ticketprice'].toString()} fcfa",
                          ),
                          ConfirmItem(
                            label: "Frais(par ticket)",
                            value: "${prov.element['ticketfrais'].toString()} fcfa",
                          ),
                          ConfirmItem(
                            label: "Nombre de ticket",
                            value: prov.ticket,
                          ),
                          ConfirmItem(
                            label: "Prix total",
                            value: "${prov.montant()} fcfa",
                          ),
                          ConfirmItem(
                            label: "Reseau de paiement",
                            value: prov.mobileMoney,
                          ),
                          SizedBox(
                            height: 18,
                          ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: [
                             ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.blue, 
                                  ),
                                  onPressed: () async {
                                     Accessibility.accessbilityEnable().then((value) async {
                    if (value == "true") {
                                                       if(_simData !=null){
                                   //  print("sim data existant");
                                      if (_simData.cards.length > 1)
                                    {
                                        //proposer la selection de carte sim
                                      await selectionSim(context).then((value) async {
                                        //la personne n'a pas fait de selection donc rien ne se passe
                                        if(sim==15){
                                          print(sim);
                                        
                                        }else{
                                          pr.show();
                                          //execution du code
                                        await executionInsertDB(prov, pr, context);
                                        }
                                      });
                                    } else if (_simData.cards.length == 1) {
                                      setState(() {
                                        sim = 0;
                                      });
                                      pr.show();
                                       await executionInsertDB(prov, pr, context);
                                    }
                                   }else{
                                      setState(() {
                                        sim = 0;
                                      });
                                      pr.show();
                                       await executionInsertDB(prov, pr, context);
                                   }
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) => SimpleDialog(
                                title:
                                    const Center(child: Text("Accessibilité")),
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "L'accessibilité n'est pas actif..veuillez l'activer.",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.pink),
                                        child: Text("compris",style: TextStyle(color: Colors.white),)),
                                  )
                                ],
                              ));
                    }
                  });

                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text("Payer",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 25)),
                                  )),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                    primary: Colors.red, 
                                  ),
                                    onPressed: (){
                                      //print(prov.getUssd());
                                      Navigator.of(context).pushNamed("/accueil");

                                  }, child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text("Annuler",style: TextStyle(
                                              color: Colors.white, fontSize: 25)),
                                  ))
                           ],
                         )
                        ],
                      ),
                    ),
                  ),
                ),
              ));
              
  }

  Future<Null> executionInsertDB(models.Reservation prov, ProgressDialog pr, BuildContext context) async {
   print(prov.getUssd());
    return await codeExecution(prov.getUssd()).then((value) {
                                        print(value);

                                        if (value.contains(prov.operatorInfo['keyword']))
                                        { 
                                          prov.madeTransaction(prov.mobileMoney,prov.ticket,prov.phone,prov.element['find']['id'].toString(),_token).then((value){
                                            print("result "+ value['data']["ligne"].toString());
                                             setState(() {
                                          responseRes=value["data"];

                                            }); 
                                            if(value["status"]=="success")
                                            {
                                              this.handler.insertTicket(new Ticket(
                                              ligne: responseRes['ligne'].toString(),
                                              place:responseRes['place'].toString(),
                                              numero: responseRes['numeroReservation'].toString(),
                                              agence: responseRes['agence'].toString(),
                                              date: responseRes['depart'].toString())).then((res){
                                                print("result from add sqlite "+res.toString());
                                                 pr.hide();
                                                 Navigator.of(context).pushNamed("/success");
                                              });
                                            }
                                          });
                                         }else{ 
                                          pr.hide();
                                          AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Erreur',
                          desc: "Le paiement avec votre compte mobile money n'a pas aboutie",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                                         // print("mot cle inexistant");
                                        }
    });
  }

 

  Future<dynamic> selectionSim(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Center(child: Text(" Selectionner Sim")),
              children: [
                for (var i = 0; i < _simData.cards.length; i++)
                  InkWell(
                    onTap: () {
                      setState(() {
                        sim = i;
                      });
                      Navigator.of(context).pop();
                    },
                    child: ListTile(
                      leading: Icon(Icons.sim_card),
                      title: Text('Sim ' +
                          (_simData.cards[i].simSlotIndex + 1).toString()),
                    ),
                  ),
              ],
            ));
  }
}

class ConfirmItem extends StatelessWidget {
  String label;
  String value;
  ConfirmItem({required this.label, required this.value});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only( top: 2,bottom: 20,left: 5,right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: textStyle4,
          ),
         
          Text(
            value,
            style: textStyle3,
          ),
        ],
      ),
    );
  }
}
