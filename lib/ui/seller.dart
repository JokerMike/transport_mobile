import 'package:awesome_dialog/awesome_dialog.dart';

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'package:sim_service/sim_service.dart';
import 'package:sim_service/models/sim_data.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sim_info/sim_info.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:progress_dialog/progress_dialog.dart';
import 'package:transport_mobile/Models/sqlite.dart';
import 'package:transport_mobile/Models/ticket.dart';
import 'package:transport_mobile/files/constantes.dart';
import 'package:transport_mobile/providers/reservation.Vendeur.dart';
import 'package:transport_mobile/ressources/shared_preferences.dart';

import 'ticketsVendeur.dart';

class Seller extends StatefulWidget {
  static const routeName = "/vendeur";
  const Seller({Key? key}) : super(key: key);

  @override
  _SellerState createState() => _SellerState();
}

class _SellerState extends State<Seller> {
  String _token = "";
  String _number = "";
  bool firstStep = true;
  bool secondStep = false;
  bool thirdStep = false;
  bool isSearching=false;
  final GlobalKey<FormState> _firstkey = GlobalKey();
  final GlobalKey<FormState> _secondkey = GlobalKey();
  String dropdownvalue = 'operateur';
  var items = ['operateur', 'Togocel', 'MoovAfrica'];
  //info
  TextEditingController dateDepart = new TextEditingController();
  TextEditingController agenceName = new TextEditingController();
  TextEditingController villeDepart = new TextEditingController();
  TextEditingController villeDestination = new TextEditingController();
  TextEditingController poids = new TextEditingController();
  TextEditingController tickets = new TextEditingController();


late DatabaseHandler handler;

void searching(){
  setState(() {
    isSearching=false;
  });
}
Future <List<dynamic> >getTicket() async{
  var tickets=await handler.listTickets();
  return tickets.toList();
}
//identity
  TextEditingController telephone = new TextEditingController();
  TextEditingController nom = new TextEditingController();
  TextEditingController prenom = new TextEditingController();
  TextEditingController code = new TextEditingController();
 static const platform = const MethodChannel('executecodeUssd');
dynamic  _simData;
  bool gettingData = false;
 //late DatabaseHandler handler;
  String _requestCode = "*555#";
  String _responseCode = "";
  String _responseMessage = "";
  int sim = 15;
   String simId="";

  dynamic responseRes;

  void getnumber() {
    SharedPreferencesClass.restore("number").then((value) {
      setState(() {
        _number = value;
      });
    });
  }

  void getToken() {
    SharedPreferencesClass.restore("token").then((value) {
      setState(() {
        _token = value;
      });
    });
  }

 Future<String> executecode(int subscriptionId, String code) async {
   
    if (await Permission.phone.request().isGranted) {
      try {
        await platform.invokeMethod("execute",
            {'subscriptionId': subscriptionId, 'code': code}).then((value) {
          setState(() {
            _responseMessage = value;
          });
        });
      } catch (e) {
        print(e);
      }
    }
    print(_responseMessage);
    return _responseMessage;
  }
  Future<void> initPlatformState() async {
   dynamic simData;

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      setState(() {
        gettingData = true; 
      });
      simData = await SimService.getSimData;
    } on PlatformException {
      print(simData);
    }
    //print(simData.carrierName);
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      gettingData = false;
      _simData = simData;
    });
  }

Future<String> codeExecution(String code) =>
      executecode(sim, code);
  @override
  void initState() {
    getnumber();
    getToken();
    this.handler=DatabaseHandler();
    initPlatformState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

     var pr = new ProgressDialog(context);
    pr.style(
        message: 'validation de paiement....',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: Image(
          image: AssetImage("images/loader.gif"),
        ));
  var resProvider = Provider.of<ReservationVendeur>(context, listen: false);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text("App Transport",style: TextStyle(color:Colors.white,)),
          centerTitle: true,
          bottom: TabBar(
            labelColor: Colors.white,
            tabs: [
              Tab(
                text: "Lancer une vente",
              ),
              Tab(
                text: "Mes ventes",
              ),
              // Tab(text: "Mon compte",),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  infoRecherche(dateDepart),
                  identite(),
                 resProvider.getAgencies().length!=0? confirmation(pr):Padding(padding: EdgeInsets.zero)
                ],
              ),
            ),
           TicketsVendeur()
          ],
        ),
      ),
    );
  }

  Visibility infoRecherche(TextEditingController dateDepart) {
    bool isloading=false;
    var resProvider = Provider.of<ReservationVendeur>(context, listen: false);
    return Visibility(
      visible: firstStep,
      child: Padding(
        padding: const EdgeInsets.only(top: 28.0, left: 12, right: 12),
        child: Form(
          key: _firstkey,
          child: Column(
            children: <Widget>[
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // SizedBox(height: 20,),
                    CircleAvatar(
                      backgroundColor: Colors.red,
                      child: Text("1"),
                    ),
                    Text(
                      "Informations de recherche",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              DateTimePicker(
                initialValue: dateDepart.text,
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                dateLabelText: 'Date de depart',
                onChanged: (val) {
                  setState(() {
                    dateDepart.text = val;
                    print("date1 :" + dateDepart.text);
                  });
                },
                validator: (val) {
                  if (dateDepart.text == "") {
                    return "ce champ est obligatoire";
                  }
                  return null;
                },
                onSaved: (val) {
                  setState(() {
                    dateDepart.text = val!;
                  });
                },
              ),
              Field(
                controller: agenceName,
                label: 'Agence de depart',
              ),
              Field(
                controller: villeDepart,
                label: "Ville de depart",
              ),
              Field(
                controller: villeDestination,
                label: "Ville de destination",
              ),
              IntField(
                label: 'Poids des bagages(en kg)',
                controller: poids,
              ),
              IntField(
                label: 'Nombre de tickets',
                controller: tickets,
              ),
              SizedBox(
                width: double.infinity, // <-- match_parent
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: isloading==false?Colors.pink:Colors.grey
                  ),
                    child: isloading==false?Text(
                     "Lancer la recherche",
                      style: textStyle4,
                    ):Text(
                     "Recherche en cours",
                      style: textStyle4,
                    ),
                    onPressed: () {
                      if (_firstkey.currentState!.validate()) {
                        setState(() {
                          isloading=true;
                        });
                        resProvider
                            .getTicket(
                                agenceName.text,
                                villeDepart.text,
                                villeDestination.text,
                                dateDepart.text,
                                tickets.text,
                                poids.text,
                                _token)
                            .then((value) {
                               setState(() {
                          isloading=false;
                        });
                          if (value == "success") {
                            showDialog(
                                context: context,
                                builder: (_) => new SimpleDialog(
                                      title: Center(
                                          child: new Text(
                                        "Informations",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 25),
                                      )),
                                      children: [
                                        displayInfo(
                                            "Agence",
                                            resProvider
                                                .getAgencies()[0]['agence']
                                                .toString()),
                                        displayInfo(
                                            "Ligne",
                                            resProvider
                                                .getAgencies()[0]['ligne']
                                                .toString()),
                                        displayInfo(
                                            "Prix du ticket",
                                            resProvider
                                                    .getAgencies()[0]['prix']
                                                    .toString() +
                                                " FCFA"),
                                        displayInfo(
                                            "Frais",
                                            resProvider
                                                    .getAgencies()[0]['frais']
                                                    .toString() +
                                                " FCFA"),
                                        displayInfo(
                                            "Place(s)", resProvider.ticket),
                                        displayInfo("Prix total",
                                            resProvider.montant() + " FCFA"),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            TextButton(
                                                child: Text('continuer',style: TextStyle(fontSize: 18,color: Colors.green),),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                setState(() {
                                              firstStep = false;
                                              secondStep = true;
                                              thirdStep = false;
                                            });
                                                }),
                                            TextButton(
                                                child: Text('Annuler',style: TextStyle(fontSize: 18,color: Colors.red)),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                  _firstkey.currentState!.reset();
                                                }),
                                          ],
                                        ),
                                        
                                      ],
                                    ));
                          } else {
                            errorDialog("Oups!!!",
                                "Désolé,pas de tickets disponibles pour ces informations.");
                          }
                        });
                      }
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget displayInfo(String label, String value) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [Text(label,style: textStyle3,), Text(value,style: textStyle3,)],
      ),
    );
  }

  Visibility identite() {
     var resProvider = Provider.of<ReservationVendeur>(context, listen: false);
    return Visibility(
        visible: secondStep,
        child: Form(
          key: _secondkey,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // SizedBox(height: 20,),
                    CircleAvatar(
                      backgroundColor: Colors.red,
                      child: Text("2"),
                    ),
                    Text(
                      "Identite et mobileInfo",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 18.0, left: 12, right: 12),
                child: TextFormField(
                  controller: telephone,
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    if (value.length != 8) {
                      return "le numero doit faire 8 chiffres";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Numero de telephone'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28.0, left: 12, right: 12),
                child: TextFormField(
                  controller: nom,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Nom du client'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28.0, left: 12, right: 12),
                child: TextFormField(
                  controller: prenom,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Prenom(s) du client'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28.0, left: 12, right: 12),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                  child: DropdownButton(
                    isExpanded: true,
                    underline: SizedBox(),
                    value: dropdownvalue,
                    //  icon: Icon(Icons.keyboard_arrow_down),
                    items: items.map((String items) {
                      return DropdownMenuItem(value: items, child: Text(items));
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dropdownvalue = newValue.toString();
                        print(dropdownvalue);
                      });
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 28.0, left: 12, right: 12),
                child: TextFormField(
                  obscureText: true,
                  keyboardType: TextInputType.number,
                  controller: code,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "ce champ est obligatoire";
                    }
                    if (value.length != 4) {
                      return "votre code secret doit etre 4 chiffres";
                    }
                    return null;
                  },
                  decoration: InputDecoration(labelText: 'Code secret de votre compte Tmoney ou flooz'),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red,
                      ),
                      onPressed: () {
                        setState(() {
                          firstStep = true;
                          secondStep = false;
                          thirdStep = false;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Retour",
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      )),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                      ),
                      onPressed: () async {
                        if (_secondkey.currentState!.validate()) {
                          if (dropdownvalue != 'operateur') {
                           showModalBottomSheet(
                            context: context,
                            builder: (context) => Container(
                                  height: 70,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 28.0),
                                    child: Row(
                                      children: [
                                        CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(Colors.blue),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Text(
                                          "Un intant",
                                          style: textStyle3,
                                        )
                                      ],
                                    ),
                                  ),
                                ));
                          resProvider.getOperatorInfo(dropdownvalue,_token).then((value){
                            Navigator.pop(context);
                            setState(() {
                          firstStep = false;
                          secondStep = false;
                          thirdStep = true;
                        });

                          });
                          } else {
                            errorDialog(
                                "Erreur", "Selectionnez un bon operateur");
                          }
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Suivant",
                            style:
                                TextStyle(color: Colors.white, fontSize: 20)),
                      )),
                ],
              )
            ],
          ),
        ));
  }

  Visibility confirmation(ProgressDialog pr) {
    var resProvider = Provider.of<ReservationVendeur>(context, listen: false);
    print(resProvider.getAgencies()[0]['agence'].toString());
    return Visibility(
        visible: thirdStep,
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // SizedBox(height: 20,),
                  CircleAvatar(
                    backgroundColor: Colors.red,
                    child: Text("3"),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "confirmation",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ConfirmItem(label: "Agence", value: resProvider.getAgencies()[0]['agence'].toString()),
            ConfirmItem(label: "Ligne", value: resProvider.getAgencies()[0]['ligne'].toString()),
            ConfirmItem(label: "Utilisateur", value: nom.text+" "+prenom.text),
            ConfirmItem(label: "Telephone", value: telephone.text),
            ConfirmItem(label: "Nombre Ticket", value: tickets.text),
            ConfirmItem(label: "Prix du ticket", value:resProvider.getAgencies()[0]['prix'].toString()+" FCFA"),
            ConfirmItem(label: "Frais du ticket", value: resProvider.getAgencies()[0]['frais'].toString()+" FCFA"),
             ConfirmItem(label: "Montant total", value: resProvider.montant()+" FCFA"),
            ConfirmItem(label: "Reseau de payement", value: dropdownvalue),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                 ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                    ),
                    onPressed: () async {
                       if(_simData !=null){
                                   //  print("sim data existant");
                                      if (_simData.cards.length > 1)
                                    {
                                        //proposer la selection de carte sim
                                      await selectionSim(context).then((value) async {
                                        //la personne n'a pas fait de selection donc rien ne se passe
                                        if(sim==15){
                                          print(sim);
                                        
                                        }else{
                                          pr.show();
                                          //execution du code
                                        await executionInsertDB(resProvider, pr, context);
                                        }
                                      });
                                    } else if (_simData.cards.length == 1) {
                                      setState(() {
                                        sim = 0;
                                      });
                                      pr.show();
                                       await executionInsertDB(resProvider, pr, context);
                                    }
                                   }else{
                                      setState(() {
                                        sim = 0;
                                      });
                                      pr.show();
                                       await executionInsertDB(resProvider,pr, context);
                                   }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("payer",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                    )),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red,
                    ),
                    onPressed: () {
                       setState(() {
                                              firstStep = false;
                                              secondStep = true;
                                              thirdStep = false;
                                            });
                  
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("retour",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                    ))
               
              ],
            )
          ],
        ));
  }

 Future<Null> executionInsertDB(ReservationVendeur prov, ProgressDialog pr, BuildContext context) async {
    print(prov.getUssd(code.text));
    return await codeExecution(prov.getUssd(code.text)).then((value) {
                                        print(value);
                                      //  prov.operatorInfo['keyword']
                                        if (value.contains("ins"))
                                        {
                                          prov.madeTransaction(dropdownvalue,tickets.text,telephone.text,prov.getAgencies()[0]['id'].toString(),_token,nom.text+" "+prenom.text).then((value){
                                           
                                            print("result "+ value['data']["ligne"].toString());
                                             setState(() {
                                          responseRes=value["data"];
                                            
                                            }); 
                                            if(value["status"]=="success")
                                            {
                                              print("ok");
                                               this.handler.insertTicket(new Ticket(
                                              ligne: responseRes['ligne'].toString(),
                                              place:responseRes['place'].toString(),
                                              numero: responseRes['numeroReservation'].toString(),
                                              agence: responseRes['agence'].toString(),
                                              date: responseRes['depart'].toString())).then((res){
                                                print("result from add sqlite "+res.toString());
                                                 pr.hide();
                                                 Navigator.of(context).pushNamed("/success");
                                              }); 
                                            }
                                          });
                                        }else{
                                          pr.hide();
                                          AwesomeDialog(
                          context: context,
                          dialogType: DialogType.ERROR,
                          headerAnimationLoop: false,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Erreur',
                          desc: "Le paiement avec votre compte mobile money n'a pas aboutie",
                          buttonsTextStyle: TextStyle(color: Colors.black),
                          showCloseIcon: true,
                         // btnOk: null,

                        )..show();
                                         // print("mot cle inexistant");
                                        }
                                      });
  }

  Future<dynamic> selectionSim(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Center(child: Text(" Selectionner Sim")),
              children: [
                for (var i = 0; i < _simData.cards.length; i++)
                  InkWell(
                    onTap: () {
                      setState(() {
                        sim = i;
                      });
                      Navigator.of(context).pop();
                    },
                    child: ListTile(
                      leading: Icon(Icons.sim_card),
                      title: Text('Sim ' +
                          (_simData.cards[i].simSlotIndex + 1).toString()),
                    ),
                  ),
              ],
            ));
  }
  Future<dynamic> errorDialog(String title, String text) {
    return showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: Center(
                  child: new Text(
                title,
                style: TextStyle(color: Colors.red, fontSize: 25),
              )),
              content: new Text(text, style: TextStyle(fontSize: 20)),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'Compris',
                    style: TextStyle(
                        color: Colors.green, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }
}

class IntField extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  const IntField({
    Key? key,
    required this.label,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: TextFormField(
        keyboardType: TextInputType.number,
        controller: controller,
        validator: (value) {
          if (value!.isEmpty) {
            return "ce champ est obligatoire";
          }
          var v = int.tryParse(value);

          if (v == null) {
            return "veuillez saisir une valeur correcte";
          }
          if (v < 0) {
            return "la valeur ne peut etre negatif";
          }
          if (v == 0) {
            return "veuillez verifier la valeur entre";
          }
          return null;
        },
        decoration: InputDecoration(labelText: label),
      ),
    );
  }
}

class Field extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  const Field({Key? key, required this.controller, required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: TextFormField(
        controller: controller,
        validator: (value) {
          if (value!.isEmpty) {
            return "ce champ est obligatoire";
          }
          return null;
        },
        decoration: InputDecoration(labelText: label),
      ),
    );
  }
}

class ConfirmItem extends StatelessWidget {
  String label;
  String value;
  ConfirmItem({required this.label, required this.value});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 20, left: 5, right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: textStyle3,
          ),
          Text(
            value,
            style: textStyle3,
          ),
        ],
      ),
    );
  }
}
