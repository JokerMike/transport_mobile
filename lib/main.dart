import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transport_mobile/common_widgets/button.dart';
import 'package:transport_mobile/providers/Auth.dart' as prov;
import 'package:transport_mobile/providers/reservation.dart' as prov;
import 'package:transport_mobile/ressources/shared_preferences.dart';
import 'package:transport_mobile/ui/home.dart' as screens;
import 'package:transport_mobile/ui/auth/loginPage.dart' as screens;
import 'package:transport_mobile/ui/seller.dart' as screens;
import 'providers/reservation.Vendeur.dart' as prov;
import 'ui/Accessibilitypage.dart' as screens;
import 'ui/auth/loginPage.dart';
import 'ui/auth/register.dart' as screens;
import 'ui/confirmPage.dart' as screens;
import 'ui/home.dart';
import 'ui/listTickets.dart' as screens;
import 'ui/payement.dart' as screens;
import 'ui/seller.dart';
import 'ui/success.dart' as screens;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
 var token;
 late String role;

 initialvue()
        {
          if(token!=null){
            // 
            if(role=="Vendeur"){
                return Seller();
            }if(role=="Voyageur"){
                return Home();
            }else{
              return screens.Login();
            }
          }else{
            return Login();
          }
        }
 initiation()async{
   await SharedPreferencesClass.restore("token").then((value) {
     setState(() {
       token=value.toString();
     });
    });

    await SharedPreferencesClass.restore("role").then((value) {
      setState(() {
         role = value.toString();
      });
    });
 }
@override
  void initState() {
    initiation();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<prov.Auth>(create: (context) => prov.Auth()),
        ChangeNotifierProvider<prov.Reservation>(create: (context) => prov.Reservation()),
        ChangeNotifierProvider<prov.ReservationVendeur>(create: (context) => prov.ReservationVendeur()),
      ],
      child: Consumer<prov.Auth>(builder: (context, auth, child) {
      auth.initValue();
       
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'E-billet',
          theme: ThemeData(
            fontFamily: 'SignikaNegative',
            primarySwatch: Colors.grey,
          ),
          home: initialvue(),
          // initialRoute: screens.Login.routeName,

          routes: {
            screens.Login.routeName: (context) => screens.Login(),
            // screens.Agencelist.routeName: (context) => screens.Agencelist(),
            screens.Home.routeName: (context) => screens.Home(),
            //screens.AddReservation.routeName: (context) =>screens.AddReservation(),
            screens.Register.routeName: (context) => screens.Register(),
            screens.Payement.routeName: (context) => screens.Payement(),
            screens.Success.routeName: (context) => screens.Success(),
            screens.Confirm.routeName: (context) => screens.Confirm(),
            screens.Seller.routeName: (context) => screens.Seller(),
            screens.ListTickets.routeName: (context) => screens.ListTickets(),
            screens.AccessibilityPage.routeName: (context) => screens.AccessibilityPage(),
          },
        );
      }),
    );
  }
  
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  String buttonText = "Incrementer";

  addNumber() {
    print(_counter);
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'You have pushed the button this many times:',
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline4,
              ),
              EBDesign(
                text: buttonText,
                press: () => {addNumber()},
              )
            ],
          ),
        ));
  }
}
