import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const String pathImage = "images/";

const Color buttonColor = Color(0xFF079585);
const Color background=Color.fromARGB(255,240,243,245);
const Color whitecolor=Colors.white;
const Color blackcolor=Colors.black;

//fromARGB(255, 177, 231, 239);
//fromRGBO(241,243,255,1);
const Color circleAvatarColor=Color.fromRGBO(69,172,255,1);
const TextStyle textStyle1=TextStyle(fontSize: 28,fontWeight:FontWeight.bold);
const TextStyle textStyle2=TextStyle(fontSize: 25,color: whitecolor,);
const TextStyle textStyle3=TextStyle(fontSize: 20,color: blackcolor);
const TextStyle textStyle4=TextStyle(fontSize: 18,color: Colors.black54,fontStyle: FontStyle.italic);
const TextStyle textStyle5=TextStyle(fontSize: 18,color: whitecolor);
