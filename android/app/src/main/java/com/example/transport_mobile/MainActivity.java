package com.example.transport_mobile;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import android.provider.Settings;


import java.util.List;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodChannel;
import ussdcontroller.USSDApi;
import ussdcontroller.USSDController;
import ussdcontroller.USSDService;

public class MainActivity extends FlutterActivity{
    private static final String CHANNEL="executecodeUssd";
   private  String resultat="he";
   private String response;
   public static boolean isAccessibilityServiceEnabled(Context context, Class<? extends AccessibilityService> service) {
    AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
    List<AccessibilityServiceInfo> enabledServices = am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK);

    for (AccessibilityServiceInfo enabledService : enabledServices) {
        ServiceInfo enabledServiceInfo = enabledService.getResolveInfo().serviceInfo;
        if (enabledServiceInfo.packageName.equals(context.getPackageName()) && enabledServiceInfo.name.equals(service.getName()))
            return true;
    }

    return false;
}

@Override
public void configureFlutterEngine(FlutterEngine flutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine);
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
    .setMethodCallHandler((call, result) -> {
        if (call.method.equals("execute")) 
        {
               final USSDApi ussdApi = USSDController.getInstance(this);
               ussdApi.callUSSDInvoke(call.argument("code"),call.argument("subscriptionId"), new USSDController.CallbackInvoke()  {
                   @Override
                   public void responseInvoke(String message) {
                    /*response=message;
                    Log.d("MON TRAITEMENT", message);*/
                   }

                   @Override
                   public void over(String message) {
                       response=message;
                       Log.d("MON TRAITEMENT",response);
               result.success(response);
                   }
               });
            //    result.success(response);
           }else if (call.method.equals("isEnable")){
               boolean enabled = isAccessibilityServiceEnabled(getApplicationContext(), USSDService.class);
            
            result.success(enabled);
               }
           else if(call.method.equals("openAccessibility")){
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
           }

        else{
            result.notImplemented();
        }

    });
}

}



