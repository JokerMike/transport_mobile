

/*
 * Copyright (c) 2020.
 * All rights reserved
 * ATL
 */
package ussdcontroller;

/**
 * Interface ussd handler
 *
 * @version 1.1.c
 * @author ATL
 * @version 1.1.c 0/4/18
 */

public interface USSDInterface {
    void sendData(String text);
}